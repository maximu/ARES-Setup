#!/usr/bin/env bash

# This file is part of ARES by The RetroArena
#
# ARES is the legal property of its developers, whose names are
# too numerous to list here. Please refer to the COPYRIGHT.md file distributed with this source.
#
# See the LICENSE.md file at the top-level directory of this distribution and
# at https://raw.githubusercontent.com/Retro-Arena/RetroArena-Setup/master/LICENSE.md
#
# Core script functionality is based upon The RetroPie Project https://retropie.org.uk Script Modules
#

rp_module_id="lr-mesen"
rp_module_desc="NES Emulator - Mesen port for libretro"
rp_module_help="ROM Extensions: .nes"
rp_module_licence="GPL3 https://github.com/libretro/mesen-libretro/blob/master/LICENSE"
rp_module_section="lr"

function sources_lr-mesen() {
    gitPullOrClone "$md_build" https://github.com/SourMesen/Mesen.git
}

function build_lr-mesen() {
    cd Libretro
    make
    md_ret_require="$md_build/Libretro/mesen_libretro.so"
}

function install_lr-mesen() {
    md_ret_files=(
        'README.md'
        '/Libretro/mesen_libretro.so'
    )
}

function configure_lr-mesen() {
   mkRomDir "nes"
    mkRomDir "fds"
    mkRomDir "famicom"
	mkRomdir "dendy"
    ensureSystemretroconfig "nes"
    ensureSystemretroconfig "fds"
    ensureSystemretroconfig "famicom"

    addEmulator 0 "$md_id" "fds" "$md_inst/mesen_libretro.so"
    addEmulator 0 "$md_id" "famicom" "$md_inst/mesen_libretro.so"
    addEmulator 0 "$md_id" "nes" "$md_inst/mesen_libretro.so"
	addEmulator 1 "$md_id" "dendy" "$md_inst/mesen_libretro.so"
    addSystem "nes"
    addSystem "fds"
    addSystem "famicom"
	addSystem "dendy"
	mkUserDir "$biosdir/HdPacks"
}