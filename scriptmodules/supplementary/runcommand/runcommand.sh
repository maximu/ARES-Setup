#!/usr/bin/env bash

# This file is part of ARES by The RetroArena
#
# ARES is the legal property of its developers, whose names are
# too numerous to list here. Please refer to the COPYRIGHT.md file distributed with this source.
#
# See the LICENSE.md file at the top-level directory of this distribution and
# at https://raw.githubusercontent.com/Retro-Arena/RetroArena-Setup/master/LICENSE.md
#
# Core script functionality is based upon The RetroPie Project https://retropie.org.uk Script Modules
#
## @file supplementary/runcommand/runcommand.sh
## @brief runcommand launching script
## @copyright GPLv3
## @details
## @par Usage
##
## `runcommand.sh VIDEO_MODE COMMAND SAVE_NAME`
##
## or
##
## `runcommand.sh VIDEO_MODE _SYS_/_PORT_ SYSTEM ROM`
##
## Video mode switching is supported on X11, KMS and Raspberry Pi (legacy graphics) systems
##
## Automatic video mode selection (all):
##
## * VIDEO_MODE = 0: use the current video mode
##
## Automatic video mode (Raspberry Pi legacy graphics):
##
## * VIDEO_MODE = 1: set video mode to 640x480 (4:3) or 720x480 (16:9) @60hz
## * VIDEO_MODE = 4: set video mode to 1024x768 (4:3) or 1280x720 (16:9) @60hz
##
## Manual video mode selection (Raspberry Pi legacy graphics):
##
## * VIDEO_MODE = "CEA-#": set video mode to CEA mode #
## * VIDEO_MODE = "DMT-#": set video mode to DMT mode #
## * VIDEO_MODE = "PAL/NTSC-RATIO": set mode to SD output with RATIO of 4:3 / 16:10 or 16:9
##
## Manual video mode selection (KMS):
##
## * VIDEO_MODE = "CRTCID-MODEID": set video mode to CRTC connector id and mode id
##
## Manual video mode selection (X11):
##
## * VIDEO_MODE = "OUTPUT:MODEID": set video mode to connected output name and mode index
##
## @note
## Video mode switching only happens if the monitor reports the modes as available
## (via tvservice) and the requested mode differs from the currently active mode
##
## If `_SYS_` or `_PORT_` is provided for the second parameter, the commandline
## will be extracted from `/opt/ares/configs/SYSTEM/emulators.cfg` with
## `%ROM%` `%BASENAME%` being replaced with the ROM parameter. This is the
## default mode used when launching in ARES so the user can switch emulator
## used as well as other options from the runcommand GUI.
##
## If SAVE_NAME is included, that is used for loading and saving of video output
## modes as well as SDL1 dispmanx settings for the current COMMAND. If omitted,
## the binary name is used as a key for the loading and saving. The savename is
## also displayed in the video output menu (detailed below), so for our purposes
## we send the emulator module id, which is somewhat descriptive yet short.
##
## On launch this script waits for 2 second for a key or joystick press. If
## pressed the GUI is shown, where a user can set video modes, default emulators
## and other options (depending what is being launched).

ROOTDIR="/opt/ares"
CONFIGDIR="$ROOTDIR/configs"
LOG="/dev/shm/runcommand.log"

RUNCOMMAND_CONF="$CONFIGDIR/all/runcommand.cfg"
VIDEO_CONF="$CONFIGDIR/all/videomodes.cfg"
EMU_CONF="$CONFIGDIR/all/emulators.cfg"
DISPMANX_CONF="$CONFIGDIR/all/dispmanx.cfg"
RETRONETPLAY_CONF="$CONFIGDIR/all/retronetplay.cfg"

# modesetting tools
TVSERVICE="/opt/vc/bin/tvservice"
KMSTOOL="$ROOTDIR/supplementary/mesa-drm/modetest"
XRANDR="xrandr"

source "$ROOTDIR/lib/inifuncs.sh"

function get_config() {
    declare -Ag MODE_MAP

    MODE_MAP[1-CEA-4:3]="CEA-1"
    MODE_MAP[1-DMT-4:3]="DMT-4"
    MODE_MAP[1-CEA-16:9]="CEA-1"

    MODE_MAP[4-CEA-4:3]="DMT-16"
    MODE_MAP[4-DMT-4:3]="DMT-16"
    MODE_MAP[4-CEA-16:9]="CEA-4"

    if [[ -f "$RUNCOMMAND_CONF" ]]; then
        iniConfig " = " '"' "$RUNCOMMAND_CONF"
        iniGet "governor"
        GOVERNOR="$ini_value"
        iniGet "use_art"
        USE_ART="$ini_value"
        iniGet "disable_joystick"
        DISABLE_JOYSTICK="$ini_value"
        iniGet "disable_menu"
        DISABLE_MENU="$ini_value"
        iniGet "image_delay"
        IMAGE_DELAY="$ini_value"
        [[ -z "$IMAGE_DELAY" ]] && IMAGE_DELAY=2
    fi

    if [[ -n "$DISPLAY" ]] && $XRANDR &>/dev/null; then
        HAS_MODESET="x11"
    # copy kms tool output to global variable to avoid multiple invocations
    elif KMS_BUFFER="$($KMSTOOL -r 2>/dev/null)"; then
        HAS_MODESET="kms"
    elif [[ -f "$TVSERVICE" ]]; then
        HAS_MODESET="tvs"
    fi
}

function start_joy2key() {
    [[ "$DISABLE_JOYSTICK" -eq 1 ]] && return
    # get the first joystick device (if not already set)
    if [[ -c "$__joy2key_dev" ]]; then
        JOY2KEY_DEV="$__joy2key_dev"
    else
        JOY2KEY_DEV="/dev/input/jsX"
    fi
    # if joy2key.py is installed run it with cursor keys for axis, and enter + tab for buttons 0 and 1
    if [[ -f "$ROOTDIR/supplementary/runcommand/joy2key.py" && -n "$JOY2KEY_DEV" ]] && ! pgrep -f joy2key.py >/dev/null; then

        # call joy2key.py: arguments are curses capability names or hex values starting with '0x'
        # see: http://pubs.opengroup.org/onlinepubs/7908799/xcurses/terminfo.html
        "$ROOTDIR/supplementary/runcommand/joy2key.py" "$JOY2KEY_DEV" kcub1 kcuf1 kcuu1 kcud1 0x0a 0x09
        JOY2KEY_PID=$(pgrep -f joy2key.py)

    # ensure coherency between on-screen prompts and actual button mapping functionality
    sleep 0.3
    fi
}

function stop_joy2key() {
    if [[ -n "$JOY2KEY_PID" ]]; then
        kill "$JOY2KEY_PID"
        JOY2KEY_PID=""
        sleep 1
    fi
}

function get_params() {
    MODE_REQ="$1"
    COMMAND="$2"

    [[ -z "$MODE_REQ" || -z "$COMMAND" ]] && return 1

    CONSOLE_OUT=0
    # if the COMMAND is _SYS_, or _PORT_ arg 3 should be system name, and arg 4 rom/game, and we look up the configured system for that combination
    if [[ "$COMMAND" == "_SYS_" || "$COMMAND" == "_PORT_" ]]; then
        # if the rom is actually a special +Start System.sh script, we should launch the script directly.
        if [[ "$4" =~ \/\+Start\ (.+)\.sh$ ]]; then
            # extract emulator from the name (and lowercase it)
            EMULATOR=${BASH_REMATCH[1],,}
            IS_SYS=0
            COMMAND="bash \"$4\""
            SYSTEM="$3"
            [[ -z "$SYSTEM" ]] && return 1
        else
            IS_SYS=1
            SYSTEM="$3"
            ROM="$4"
            ROM_BN_EXT="${ROM##*/}"
            ROM_BN="${ROM_BN_EXT%.*}"
            if [[ "$COMMAND" == "_PORT_" ]]; then
                CONF_ROOT="$CONFIGDIR/ports/$SYSTEM"
                EMU_SYS_CONF="$CONF_ROOT/emulators.cfg"
                IS_PORT=1
            else
                CONF_ROOT="$CONFIGDIR/$SYSTEM"
                EMU_SYS_CONF="$CONF_ROOT/emulators.cfg"
                IS_PORT=0
            fi
            SYS_SAVE_ROM_OLD="a$(echo "$SYSTEM$ROM" | md5sum | cut -d" " -f1)"
            SYS_SAVE_ROM="$(clean_name "${SYSTEM}_${ROM_BN}")"
            [[ -z "$SYSTEM" ]] && return 1
            get_sys_command
        fi
    else
        IS_SYS=0
        CONSOLE_OUT=1
        EMULATOR="$3"
        # if we have an emulator name (such as module_id) we use that for storing/loading parameters for video output/dispmanx
        # if the parameter is empty we use the name of the binary (to avoid breakage with out of date emulationstation configs)
        [[ -z "$EMULATOR" ]] && EMULATOR="${COMMAND/% */}"
    fi

    NETPLAY=0
    return 0
}

function clean_name() {
    local name="$1"
    name="${name//\//_}"
    name="${name//[^a-zA-Z0-9_\-]/}"
    echo "$name"
}

function set_save_vars() {
    # convert emulator name / binary to a names usable as variables in our config files
    SAVE_EMU="$(clean_name "$EMULATOR")"
    SAVE_ROM_OLD=r$(echo "$COMMAND" | md5sum | cut -d" " -f1)
    if [[ "$IS_SYS" -eq 1 ]]; then
        SAVE_ROM="${SAVE_EMU}_$(clean_name "$ROM_BN")"
    else
        SAVE_ROM="$SAVE_EMU"
    fi
}

function get_all_tvs_modes() {
    declare -Ag MODE
    local group
    for group in CEA DMT; do
        while read -r line; do
            local id="$(echo "$line" | grep -oE "mode [0-9]*" | cut -d" " -f2)"
            local info="$(echo "$line" | cut -d":" -f2-)"
            info=${info/ /}
            if [[ -n "$id" ]]; then
                MODE_ID+=($group-$id)
                MODE[$group-$id]="$info"
            fi
        done < <($TVSERVICE -m $group)
    done
    local aspect
    for group in "NTSC" "PAL"; do
        for aspect in "4:3" "16:10" "16:9"; do
            MODE_ID+=($group-$aspect)
            MODE[$group-$aspect]="SDTV - $group-$aspect"
        done
    done
}

function get_all_kms_modes() {
    declare -Ag MODE
    local default_mode="$(echo "$KMS_BUFFER" | grep -Em1 "^Mode:.*(driver|userdef).*crtc")"
    local crtc="$(echo "$default_mode" | awk '{ print $(NF-1) }')"
    local crtc_encoder="$(echo "$KMS_BUFFER" | grep "Encoder map:" | awk -v crtc="$crtc" '$5 == crtc { print $3 }')"

    local info
    local line
    local mode_id

    # add default mode as fallback in case real mode cannot be mapped
    MODE[def-def]="$(echo "$default_mode" | awk '{--NF --NF --NF; print}' | cut -c7-)"

    while read -r line; do
        # encoder id
        encoder_id="$(echo "$line" | awk '{ print $(NF-1) }')"

        # only match encoders that are linked to the currently active crtc
        if [[ "$encoder_id" == "$crtc_encoder" ]]; then
            # mode id
            mode_id="$(echo "$line" | awk '{ print $NF }')"

            # make output more human-readable
            info="$(echo "$line" | awk '{--NF --NF --NF; print}' | cut -c7-)"

            # populate resolution into arrays (using mapped crtc encoder value)
            MODE_ID+=($crtc-$mode_id)
            MODE[$crtc-$mode_id]="$info"

            # if string matches default mode, add a special mapped entry
            [[ "$default_mode" =~ "$info" ]] && MODE[map-map]="$crtc $mode_id"
        fi
    done < <(echo "$KMS_BUFFER" | grep "Mode:" | grep "connector")
}

function get_all_x11_modes()
{
        declare -Ag MODE
        local id
        local info
        local line
        local verbose_info=()
        local output="$($XRANDR --verbose | grep " connected" | awk '{ print $1 }')"

        while read -r line; do
            # scan for line that contains bracketed mode id
            id="$(echo "$line" | awk '{ print $2 }' | grep "([0-9]\{1,\}x[0-9]\{1,\})")"

            if [[ -n "$id" ]]; then
                # strip brackets from mode id
                id="$(echo ${id:1:-1})"

                # extract extended details
                verbose_info=($(echo "$line" | awk '{ for (i=3; i<=NF; ++i) print $i }'))

                # extract x/y resolution, vertical refresh rate and append details
                read -r line
                info="$(echo "$line" | awk '{ print $3 }')"
                read -r line
                info+="x$(echo "$line" | awk '{ print $3 }') @ $(echo "$line" | awk '{ print $NF }') ("${verbose_info[*]}")"

                # populate resolution into arrays
                MODE_ID+=($output:$id)
                MODE[$output:$id]="$info"
            fi
        done < <($XRANDR --verbose)
}

function get_tvs_mode_info() {
    local status="$($TVSERVICE -s)"
    local temp
    local mode_info=()

    # get mode type / id
    if [[ "$status" =~ (PAL|NTSC) ]]; then
        temp=($(echo "$status" | grep -oE "(PAL|NTSC) (4:3|16:10|16:9)"))
    else
        temp=($(echo "$status" | grep -oE "(CEA|DMT) \([0-9]+\)"))
    fi
    mode_info[0]="${temp[0]}"
    mode_info[1]="${temp[1]//[()]/}"

    # get mode resolution
    temp=$(echo "$status" | cut -d"," -f2 | grep -oE "[0-9]+x[0-9]+")
    temp=(${temp/x/ })
    mode_info[2]="${temp[0]}"
    mode_info[3]="${temp[1]}"

    # get aspect ratio
    temp=$(echo "$status" | grep -oE "([0-9]+:[0-9]+)")
    mode_info[4]="$temp"

    # get refresh rate
    temp=$(echo "$status" | grep -oE "[0-9\.]+Hz" | cut -d"." -f1)
    mode_info[5]="$temp"

    echo "${mode_info[@]}"
}

function get_kms_mode_info() {
    local mode_id=(${1/-/ })
    local mode_info=()
    local status

    if [[ -z "${mode_id[*]}" ]]; then
	if [[ -n "${MODE[map-map]}" ]]; then
            # use mapped mode directly
            mode_id=(${MODE[map-map]})
        else
            # use fallback mode
            mode_id=(def def)
        fi
    fi

    # split resolution
    status=(${MODE[${mode_id[0]}-${mode_id[1]}]/x/ })

    # get crtc id
    mode_info[0]="${mode_id[0]}"

    # get mode id
    mode_info[1]="${mode_id[1]}"

    # get mode resolution
    mode_info[2]="${status[0]}"
    mode_info[3]="${status[1]}"

    # get aspect ratio
    mode_info[4]="${status[5]}"

    # get refresh rate
    mode_info[5]="${status[3]}"

    echo "${mode_info[@]}"
}

function get_x11_mode_info() {
    local mode_id=(${1/:/ })
    local mode_info=()
    local status

    if [[ -z "$mode_id" ]]; then
        # determine current output
        mode_id[0]="$($XRANDR --verbose | grep " connected" | awk '{ print $1 }')"
        # determine current mode id & strip brackets
        mode_id[1]="$($XRANDR --verbose | grep " connected" | grep -o "([0-9]\{1,\}x[0-9]\{1,\})")"
        mode_id[1]="$(echo ${mode_id[1]:1:-1})"
    fi

    # mode type corresponds to the currently connected output name
    mode_info[0]="${mode_id[0]}"

    # get mode id
    mode_info[1]="${mode_id[1]}"

    # get status line and split resolution
    status=(${MODE[${mode_id[0]}:${mode_id[1]}]/x/ })

    # get resolution
    mode_info[2]="${status[0]}"
    mode_info[3]="${status[1]}"

    # aspect ratio cannot be determined for X11
    mode_info[4]="n/a"

    # get refresh rate (stripping Hz, rounded to integer)
    mode_info[5]="$(LC_NUMERIC=C printf '%.0f\n' ${status[3]::-2})"

    echo "${mode_info[@]}"
}

function default_process() {
    local config="$1"
    local mode="$2"
    local key="$3"
    local value="$4"

    iniConfig " = " '"' "$config"
    case "$mode" in
        get)
            iniGet "$key"
            echo "$ini_value"
            ;;
        set)
            iniSet "$key" "$value"
            ;;
        del)
            iniDel "$key"
            ;;
    esac
}

function default_mode() {
    local mode="$1"
    local type="$2"
    local value="$3"

    local key
    case "$type" in
        vid_emu)
            key="$SAVE_EMU"
            ;;
        vid_rom_old)
            key="$SAVE_ROM_OLD"
            ;;
        vid_rom)
            key="$SAVE_ROM"
            ;;
        fb_emu)
            key="${SAVE_EMU}_fb"
            ;;
        fb_rom_old)
            key="${SAVE_ROM_OLD}_fb"
            ;;
        fb_rom)
            key="${SAVE_ROM}_fb"
            ;;
        render)
            key="${SAVE_EMU}_render"
            ;;
    esac
    default_process "$VIDEO_CONF" "$mode" "$key" "$value"
}

function default_emulator() {
    local mode="$1"
    local type="$2"
    local value="$3"

    local key
    local config="$EMU_SYS_CONF"

    case "$type" in
        emu_sys)
            key="default"
            ;;
        emu_cmd)
            key="$EMULATOR"
            ;;
        emu_rom_old)
            key="$SYS_SAVE_ROM_OLD"
            config="$EMU_CONF"
            ;;
        emu_rom)
            key="$SYS_SAVE_ROM"
            config="$EMU_CONF"
            ;;
    esac
    default_process "$config" "$mode" "$key" "$value"
}

function load_mode_defaults() {
    local separator="-"
    [[ "$HAS_MODESET" == "x11" ]] && separator=":"
    local temp
    MODE_ORIG=()


    if [[ -n "$HAS_MODESET" ]]; then
        # populate available modes
        [[ -z "$MODE_ID" ]] && get_all_${HAS_MODESET}_modes

        # get current mode / aspect ratio
        MODE_ORIG=($(get_${HAS_MODESET}_mode_info))
        MODE_CUR=("${MODE_ORIG[@]}")
        MODE_ORIG_ID="${MODE_ORIG[0]}${separator}${MODE_ORIG[1]}"

       if [[ "$MODE_REQ" == "0" ]]; then
            MODE_REQ_ID="$MODE_ORIG_ID"
       elif [[ "$HAS_MODESET" == "tvs" ]]; then
            # get default mode for requested mode of 1 or 4
            if [[ "$MODE_REQ" =~ (1|4) ]]; then
                # if current aspect is anything else like 5:4 / 10:9 just choose a 4:3 mode
                local aspect="${MODE_ORIG[4]}"
                [[ "$aspect" =~ (4:3|16:9) ]] || aspect="4:3"
                temp="${MODE_REQ}-${MODE_ORIG[0]}-$aspect"
                MODE_REQ_ID="${MODE_MAP[$temp]}"
            else
                MODE_REQ_ID="$MODE_REQ"
            fi
        else
            MODE_REQ_ID="$MODE_REQ"
        fi
    fi

    # get default fb_res (if not running on X)
    FB_ORIG=()
    if [[ -z "$DISPLAY" ]]; then
        local status=($(fbset | tr -s '\n'))
        FB_ORIG[0]="${status[3]}"
        FB_ORIG[1]="${status[4]}"
        FB_ORIG[2]="${status[7]}"
    fi

    # default retroarch render res to config file
    RENDER_RES="config"

    local mode
    if [[ -f "$VIDEO_CONF" ]]; then
        # load default video mode for emulator / rom
        mode="$(default_mode get vid_emu)"
        [[ -n "$mode" ]] && MODE_REQ_ID="$mode"

        # get default mode for system + rom combination
        # try the old key first and convert to the new key if found
        mode="$(default_mode get vid_rom_old)"
        if [[ -n "$mode" ]]; then
            default_mode del vid_rom_old
            default_mode set vid_rom "$mode"
            MODE_REQ_ID="$mode"
        else
            mode="$(default_mode get vid_rom)"
            [[ -n "$mode" ]] && MODE_REQ_ID="$mode"
        fi

        if [[ "$HAS_MODESET" == "tvs" ]]; then
            # load default framebuffer res for emulator / rom
            mode="$(default_mode get fb_emu)"
            [[ -n "$mode" ]] && FB_NEW="$mode"

            # get default fb mode for system + rom combination
            # try the old key first and convert to the new key if found
            mode="$(default_mode get fb_rom_old)"
            if [[ -n "$mode" ]]; then
                default_mode del fb_rom_old
                default_mode set fb_rom "$mode"
                FB_NEW="$mode"
            else
                mode="$(default_mode get fb_rom)"
                [[ -n "$mode" ]] && FB_NEW="$mode"
            fi
        fi

        # get default retroarch render resolution for emulator
        mode="$(default_mode get render)"
        [[ -n "$mode" ]] && RENDER_RES="$mode"
    fi
}

function main_menu() {
    local save
    local cmd
    local choice

    local user_menu=0
    [[ -d "$CONFIGDIR/all/runcommand-menu" && -n "$(find "$CONFIGDIR/all/runcommand-menu" -maxdepth 1 -name "*.sh")" ]] && user_menu=1

    [[ -z "$ROM_BN" ]] && ROM_BN="game/rom"
    [[ -z "$SYSTEM" ]] && SYSTEM="emulator/port"

    while true; do

        local options=()
        if [[ "$IS_SYS" -eq 1 ]]; then
            local emu_sys="$(default_emulator get emu_sys)"
            local emu_rom="$(default_emulator get emu_rom)"
            options+=(
                1 "Select default emulator for $SYSTEM ($emu_sys)"
                2 "Select emulator for ROM ($emu_rom)"
            )
            [[ -n "$emu_rom" ]] && options+=(3 "Remove emulator choice for ROM")
        fi

        if [[ -n "$HAS_MODESET" ]]; then
            local vid_emu="$(default_mode get vid_emu)"
            local vid_rom="$(default_mode get vid_rom)"
            options+=(
                4 "Select default video mode for $EMULATOR ($vid_emu)"
                5 "Select video mode for $EMULATOR + rom ($vid_rom)"
            )
            [[ -n "$vid_emu" ]] && options+=(6 "Remove video mode choice for $EMULATOR")
            [[ -n "$vid_rom" ]] && options+=(7 "Remove video mode choice for $EMULATOR + ROM")
        fi

        if [[ "$EMULATOR" == lr-* ]]; then
            if [[ "$HAS_MODESET" == "tvs" ]]; then
                options+=(8 "Select RetroArch render res for $EMULATOR ($RENDER_RES)")
            fi
            options+=(9 "Edit custom RetroArch config for this ROM")
        elif [[ "$HAS_MODESET" == "tvs" ]]; then
            local fb_emu="$(default_mode get fb_emu)"
            local fb_rom="$(default_mode get fb_rom)"
            options+=(
                10 "Select framebuffer res for $EMULATOR ($fb_emu)"
                11 "Select framebuffer res for $EMULATOR + ROM ($fb_rom)"
            )
            [[ -n "$fb_emu" ]] && options+=(12 "Remove framebuffer res choice for $EMULATOR")
            [[ -n "$fb_rom" ]] && options+=(13 "Remove framebuffer res choice for $EMULATOR + ROM")
        fi

        options+=(X "Launch")

        if [[ "$EMULATOR" == lr-* ]]; then
            options+=(L "Launch with verbose logging")
            options+=(Z "Launch with netplay enabled")
        fi

        if [[ "$user_menu" -eq 1 ]]; then
            options+=(U "User Menu")
        fi

        options+=(Q "Exit (without launching)")

        local temp_mode
        if [[ -n "$HAS_MODESET" ]]; then
            temp_mode="${MODE[$MODE_REQ_ID]}"
        else
            temp_mode="n/a"
        fi
        cmd=(dialog --nocancel --menu "System: $SYSTEM\nEmulator: $EMULATOR\nVideo Mode: $temp_mode\nROM: $ROM_BN"  22 76 16 )
        choice=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)
        case "$choice" in
            1)
                choose_emulator "emu_sys" "$emu_sys"
                ;;
            2)
                choose_emulator "emu_rom" "$emu_rom"
                ;;
            3)
                default_emulator "del" "emu_rom"
                get_sys_command
                set_save_vars
                load_mode_defaults
                ;;
            4)
                choose_mode "vid_emu" "$vid_emu"
                ;;
            5)
                choose_mode "vid_rom" "$vid_rom"
                ;;
            6)
                default_mode "del" "vid_emu"
                load_mode_defaults
                ;;
            7)
                default_mode "del" "vid_rom"
                load_mode_defaults
                ;;
            8)
                choose_render_res "render" "$RENDER_RES"
                ;;
            9)
                touch "$ROM.cfg"
                cmd=(dialog --editbox "$ROM.cfg" 22 76)
                choice=$("${cmd[@]}" 2>&1 >/dev/tty)
                [[ -n "$choice" ]] && echo "$choice" >"$ROM.cfg"
                [[ ! -s "$ROM.cfg" ]] && rm "$ROM.cfg"
                ;;
            10)
                choose_fb_res "fb_emu" "$fb_emu"
                ;;
            11)
                choose_fb_res "fb_rom" "$fb_rom"
                ;;
            12)
                default_mode "del" "fb_emu"
                load_mode_defaults
                ;;
            13)
                default_mode "del" "fb_rom"
                load_mode_defaults
                ;;
            Z)
                NETPLAY=1
                break
                ;;
            X)
                return 0
                ;;
            L)
                COMMAND+=" --verbose"
                return 0
                ;;
            U)
                user_menu
                local ret="$?"
                [[ "$ret" -eq 1 ]] && return 1
                [[ "$ret" -eq 2 ]] && return 0
                ;;
            Q)
                return 1
                ;;
        esac
    done
    return 0
}

function choose_mode() {
    local mode="$1"
    local default="$2"

    local options=()
    local key
    for key in "${MODE_ID[@]}"; do
        options+=("$key" "${MODE[$key]}")
    done
    local cmd=(dialog --default-item "$default" --menu "Choose video output mode"  22 76 16 )
    local choice=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)
    [[ -z "$choice" ]] && return

    default_mode set "$mode" "$choice"
    load_mode_defaults
}

function choose_emulator() {
    local mode="$1"
    local default="$2"
    local cancel="$3"

    local default
    local default_id

    local options=()
    local i=1
    while read line; do
        # convert key=value to array
        local line=(${line/=/ })
        local id=${line[0]}
        [[ "$id" == "default" ]] && continue
        local apps[$i]="$id"
        if [[ "$id" == "$default" ]]; then
            default_id="$i"
        fi
        options+=($i "$id")
        ((i++))
    done < <(sort "$EMU_SYS_CONF")
    if [[ -z "${options[*]}" ]]; then
        dialog --msgbox "No emulator options found for $SYSTEM - Do you have a valid $EMU_SYS_CONF ?" 20 60 >/dev/tty
        stop_joy2key
        exit 1
    fi
    local cmd=(dialog $cancel --default-item "$default_id" --menu "Choose default emulator"  22 76 16 )
    local choice=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)
    [[ -z "$choice" ]] && return

    default_emulator set "$mode" "${apps[$choice]}"
    get_sys_command
    set_save_vars
    load_mode_defaults
}

function get_resolutions() {
    local res=(
        "320x224"
        "320x240"
        "400x240"
        "480x320"
        "640x480"
        "720x480"
        "720x576"
        "800x480"
        "800x600"
        "960x720"
        "1024x600"
        "1024x768"
        "1024x800"
        "1280x720"
        "1280x800"
        "1280x960"
        "1280x1024"
        "1360x768"
        "1366x768"
        "1920x1080"
    )
    echo "${res[@]}"
}

function choose_render_res() {
    local mode="$1"
    local default="$2"

    local res=($(get_resolutions))
    local i=1
    local item
    local options=()
    for item in "${res[@]}"; do
        [[ "$item" == "$default" ]] && default="$i"
        options+=($i "$item")
        ((i++))
    done
    options+=(
        O "Use video output resolution"
        C "Use config file resolution"
    )
    [[ "$default" == "output" ]] && default="O"
    [[ "$default" == "config" ]] && default="C"
    local cmd=(dialog --default-item "$default" --menu "Choose RetroArch render resolution" 22 76 16 )
    local choice=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)
    [[ -z "$choice" ]] && return
    case "$choice" in
        O)
            choice="output"
            ;;
        C)
            choice="config"
            ;;
        *)
            choice="${res[$choice-1]}"
            ;;
    esac

    default_mode set "$mode" "$choice"
    load_mode_defaults
}

function choose_fb_res() {
    local mode="$1"
    local default="$2"

    local res=($(get_resolutions))
    local i=1
    local item
    local options=()
    for item in "${res[@]}"; do
        options+=($i "$item")
        ((i++))
    done
    local cmd=(dialog --default-item "$default" --menu "Choose framebuffer resolution (Useful for X and console apps)" 22 76 16 )
    local choice=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)
    [[ -z "$choice" ]] && return
    choice="${res[$choice-1]}"

    default_mode set "$mode" "$choice"
    load_mode_defaults
}

function user_menu() {
    local default
    local options=()
    local script
    local i=1
    while read -r script; do
        script="${script##*/}"
        script="${script%.*}"
        options+=($i "$script")
        ((i++))
    done < <(find "$CONFIGDIR/all/runcommand-menu" -type f -name "*.sh" | sort)
    local default
    local cmd
    local choice
    local ret
    while true; do
        cmd=(dialog --default-item "$default" --cancel-label "Back" --menu "Choose option"  22 76 16)
        choice=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)
        [[ -z "$choice" ]] && return 0
        default="$choice"
        script="runcommand-menu/${options[choice*2-1]}.sh"
        user_script "$script"
        ret="$?"
        [[ "$ret" -eq 1 || "$ret" -eq 2 ]] && return "$ret"
    done
}

function switch_fb_res() {
    local res=(${1/x/ })
    local res_x="${res[0]}"
    local res_y="${res[1]}"
    local depth="$2"
    [[ -z "$depth" ]] && depth="${FB_ORIG[2]}"

    if [[ -z "$res_x" || -z "$res_y" ]]; then
        fbset --all -depth 8
        fbset --all -depth $depth
    else
        fbset --all -depth 8
        fbset --all --geometry $res_x $res_y $res_x $res_y $depth
    fi
}

function build_xinitrc() {
    local mode="$1"
    local xinitrc="/dev/shm/ares_xinitrc"

    case "$mode" in
        clear)
            rm -rf "$xinitrc"
            ;;
        build)
            echo "#!/bin/bash" >"$xinitrc"

            # do modesetting (if supported)
            if [[ -n "$HAS_MODESET" ]]; then
                cat >>"$xinitrc" <<_EOF_
XRANDR_OUTPUT="\$($XRANDR --verbose | grep " connected" | awk '{ print \$1 }')"
$XRANDR --output \$XRANDR_OUTPUT --mode ${MODE_CUR[2]}x${MODE_CUR[3]} --refresh ${MODE_CUR[5]}
echo "Set mode ${MODE_CUR[2]}x${MODE_CUR[3]}@${MODE_CUR[5]}Hz on \$XRANDR_OUTPUT"
_EOF_
            fi

            # echo command line for runcommand log
            cat >>"$xinitrc" <<_EOF_
echo -e "\nExecuting (via xinit): "${COMMAND//\$/\\\$}"\n"
${COMMAND//\$/\\\$}
_EOF_
            chmod +x "$xinitrc"

            # rewrite command to launch our xinit script (if not startx)
            if ! [[ "$COMMAND" =~ ^startx ]]; then
                COMMAND="xinit $xinitrc"
            fi

            # workaround for launching xserver on correct/user owned tty
            # see https://github.com/RetroPie/RetroPie-Setup/issues/1805

            # if no TTY env var is set, try and get it - eg if launching a ports script or runcommand manually
            if [[ -z "$TTY" ]]; then
                TTY=$(tty)
                TTY=${TTY:8:1}
            fi

            # if we managed to get the current tty then try and use it
            if [[ -n "$TTY" ]]; then
                COMMAND="$COMMAND -- vt$TTY -keeptty"
            fi
            ;;
    esac
}

function mode_switch() {
    local command_prefix
    local separator="-"
    # X11 uses hypens in connector names
    [[ $HAS_MODESET == "x11" ]] && separator=":"
    local mode_id=(${1/${separator}/ })

    # if the requested mode is the same as the current mode, don't switch
    [[ "${mode_id[*]}" == "${MODE_CUR[0]} ${MODE_CUR[1]}" ]] && return 1

    if [[ "$HAS_MODESET" == "kms" ]]; then
        # update the target resolution even though the underlying fb hasn't changed
        MODE_CUR=($(get_${HAS_MODESET}_mode_info "${mode_id[*]}"))
        # inject the environment variables to do modesetting for SDL2 applications
        command_prefix="SDL_VIDEO_KMSDRM_CRTCID=${MODE_CUR[0]} SDL_VIDEO_KMSDRM_MODEID=${MODE_CUR[1]}"
        COMMAND="$(echo "$command_prefix $COMMAND" | sed -e "s/;/; $command_prefix /g")"

        return 0
    elif [[ "$HAS_MODESET" == "x11" ]]; then
        # query the target resolution
        MODE_CUR=($(get_${HAS_MODESET}_mode_info "${mode_id[*]}"))
        # set target resolution
        $XRANDR --output "${MODE_CUR[0]}" --mode "${MODE_CUR[1]}"

        [[ "$?" -eq 0 ]] && return 0
    elif [[ "$HAS_MODESET" == "tvs" ]]; then
        if [[ "${mode_id[0]}" == "PAL" ]] || [[ "${mode_id[0]}" == "NTSC" ]]; then
            $TVSERVICE -c "${mode_id[*]}" >/dev/null
        else
            $TVSERVICE -e "${mode_id[*]}" >/dev/null
        fi

        # if we have switched mode, switch the framebuffer resolution also
        if [[ "$?" -eq 0 ]]; then
            sleep 1
            MODE_CUR=($(get_${HAS_MODESET}_mode_info))
            [[ -z "$FB_NEW" ]] && FB_NEW="${MODE_CUR[2]}x${MODE_CUR[3]}"
            return 0
        fi
    fi

    return 1
}

function restore_fb() {
    sleep 1
    switch_fb_res "${FB_ORIG[0]}x${FB_ORIG[1]}" "${FB_ORIG[2]}"
}

function config_dispmanx() {
    # if we are running under X then don't try and use dispmanx
    [[ -n "$DISPLAY" || "$XINIT" -eq 1 ]] && return
    local name="$1"
    # if we have a dispmanx conf file and $name is in it (as a variable) and set to 1,
    # change the library path to load dispmanx sdl first
    if [[ -f "$DISPMANX_CONF" ]]; then
        iniConfig " = " '"' "$DISPMANX_CONF"
        iniGet "$name"
        [[ "$ini_value" == "1" ]] && COMMAND="SDL1_VIDEODRIVER=dispmanx $COMMAND"
    fi
}

function retroarch_append_config() {
    local conf="/dev/shm/retroarch.cfg"
    local dim

    # only for retroarch emulators
    [[ "$EMULATOR" != lr-* ]] && return

    # make sure tmp folder exists for unpacking archives
    mkdir -p "/tmp/retroarch"

    rm -f "$conf"
    touch "$conf"
    iniConfig " = " '"' "$conf"

    if [[ -n "$HAS_MODESET" && "${MODE_CUR[5]}" -gt 0 ]]; then
        # set video_refresh_rate in our config to the same as the screen refresh
        iniSet "video_refresh_rate" "${MODE_CUR[5]}"
    fi

    # populate with target resolution & fullscreen flag if KMS is active
    if [[ "$HAS_MODESET" != "tvs" ]]; then
        iniSet "video_fullscreen" "true"
        iniSet "video_fullscreen_x" "${MODE_CUR[2]}"
        iniSet "video_fullscreen_y" "${MODE_CUR[3]}"
    # if our render resolution is "config", then we don't set anything (use the value in the retroarch.cfg)
    elif [[ "$RENDER_RES" != "config" ]]; then
        if [[ "$RENDER_RES" == "output" ]]; then
            dim=(0 0)
        else
            dim=(${RENDER_RES/x/ })
        fi
        iniSet "video_fullscreen_x" "${dim[0]}"
        iniSet "video_fullscreen_y" "${dim[1]}"
    fi

    # if the ROM has a custom configuration then append that too
    if [[ -f "$ROM.cfg" ]]; then
        conf+="'|'\"$ROM.cfg\""
    fi

    # if we already have an existing appendconfig parameter, we need to add our configs to that
    if [[ "$COMMAND" =~ "--appendconfig" ]]; then
        COMMAND=$(echo "$COMMAND" | sed "s#\(--appendconfig *[^ $]*\)#\1'|'$conf#")
    else
        COMMAND+=" --appendconfig $conf"
    fi

    # append any NETPLAY configuration
    if [[ "$NETPLAY" -eq 1 ]] && [[ -f "$RETRONETPLAY_CONF" ]]; then
        source "$RETRONETPLAY_CONF"
        COMMAND+=" -$__netplaymode $__netplayhostip_cfile --port $__netplayport --nick $__netplaynickname"
    fi
}

function set_governor() {
    governor_old=()
    # we save the previous states first, as setting any cpuX on the RPI will also set the value for the other cores
    # which would cause us to save the wrong state for cpu1/2/3 after setting cpu0. On the RPI we could just process
    # cpu0, but this code needs to work on other platforms that do support a "per core" CPU governor.
    for cpu in /sys/devices/system/cpu/cpu[0-9]*/cpufreq/scaling_governor; do
        governor_old+=($(<$cpu))
    done
    for cpu in /sys/devices/system/cpu/cpu[0-9]*/cpufreq/scaling_governor; do
        echo "$1" | sudo tee "$cpu" >/dev/null
    done
}

function restore_governor() {
    local i=0
    for cpu in /sys/devices/system/cpu/cpu[0-9]*/cpufreq/scaling_governor; do
        echo "${governor_old[$i]}" | sudo tee "$cpu" >/dev/null
        ((i++))
    done
}

function get_sys_command() {
    if [[ ! -f "$EMU_SYS_CONF" ]]; then
        echo "No config found for system $SYSTEM"
        stop_joy2key
        exit 1
    fi

    # get system & rom specific emulator if set
    local emulator="$(default_emulator get emu_sys)"
    if [[ -z "$emulator" ]]; then
        echo "No default emulator found for system $SYSTEM"
        start_joy2key
        choose_emulator "emu_sys" "" "--nocancel"
        stop_joy2key
        get_sys_command "$SYSTEM" "$ROM"
        return
    fi
    EMULATOR="$emulator"

    # get default emulator for system + rom combination
    # try the old key first and convert to the new key if found
    emulator="$(default_emulator get emu_rom_old)"

    if [[ -n "$emulator" ]]; then
        default_emulator del emu_rom_old
        default_emulator set emu_rom "$emulator"
        EMULATOR="$emulator"
    else
        emulator="$(default_emulator get emu_rom)"
        [[ -n "$emulator" ]] && EMULATOR="$emulator"
    fi

    COMMAND="$(default_emulator get emu_cmd)"

    # replace tokens
    COMMAND="${COMMAND//\%ROM\%/\"$ROM\"}"
    COMMAND="${COMMAND//\%BASENAME\%/\"$ROM_BN\"}"

    # special case to get the last 2 folders for quake games for the -game parameter
    # remove everything up to /quake/
    local quake_dir="${ROM##*/quake/}"
    # remove filename
    quake_dir="${quake_dir%/*}"
    COMMAND="${COMMAND//\%QUAKEDIR\%/\"$quake_dir\"}"

    # if it starts with CON: it is a console application (so we don't redirect stdout later)
    if [[ "$COMMAND" == CON:* ]]; then
        # remove CON:
        COMMAND="${COMMAND:4}"
        CONSOLE_OUT=1
    fi

    # if it starts with XINIT: it is an X11 application (so we need to launch via xinit)
    if [[ "$COMMAND" == XINIT:* ]]; then
        # remove XINIT:
        COMMAND="${COMMAND:6}"
        XINIT=1
    fi
}

function show_launch() {
    local images=()

    if [[ "$IS_SYS" -eq 1 && "$USE_ART" -eq 1 ]]; then
        # if using art look for images in paths for es art.
        images+=(
            "$HOME/ARES/roms/$SYSTEM/images/${ROM_BN}-image"
            "$HOME/.emulationstation/downloaded_images/$SYSTEM/${ROM_BN}-image"
        )
    fi

    # look for custom launching images and videos
    if [[ "$IS_SYS" -eq 1 ]]; then
        images+=(
            "$HOME/ARES/roms/$SYSTEM/images/${ROM_BN}-launching"
            "$CONF_ROOT/launching"
        )
    fi
    [[ "$IS_PORT" -eq 1 ]] && images+=("$CONFIGDIR/ports/launching")
    images+=("$CONFIGDIR/all/launching")

    local image
    local path
    local ext
    for path in "${images[@]}"; do
        for ext in jpg png; do
            if [[ -f "$path.$ext" ]]; then
                image="$path.$ext"
                break 2
            fi
        done
    done

    if [[ -n "$image" ]]; then
        # if we are running under X use feh otherwise try and use fbi
        if [[ -n "$DISPLAY" ]]; then
            feh -F -N -Z -Y -q "$image" & &>/dev/null
            IMG_PID=$!
            sleep "$IMAGE_DELAY"
        else
            LAUNCHVIDS="$HOME/ARES/launchingvideos"
            if [[ -e "$HOME/.config/launchingvideos" ]]; then
                if [[ -e "$LAUNCHVIDS/system-$SYSTEM.mp4" ]]; then
                    if grep -q "ODROID-N2" /sys/firmware/devicetree/base/model 2>/dev/null; then
                        mpv -really-quiet -vo sdl -fs "$LAUNCHVIDS/system-$SYSTEM.mp4" </dev/tty &>/dev/null
                    elif grep -q "RockPro64" /sys/firmware/devicetree/base/model 2>/dev/null; then
                        mpv -really-quiet -vo sdl -fs "$LAUNCHVIDS/system-$SYSTEM.mp4" </dev/tty &>/dev/null
                    else
                        mpv -slave -nogui -really-quiet -vo sdl -fs -zoom "$LAUNCHVIDS/system-$SYSTEM.mp4" </dev/tty &>/dev/null
                    fi
                elif [[ -e "$LAUNCHVIDS/system-default.mp4" ]]; then
                    if grep -q "ODROID-N2" /sys/firmware/devicetree/base/model 2>/dev/null; then
                        mpv -really-quiet -vo sdl -fs "$LAUNCHVIDS/system-default.mp4" </dev/tty &>/dev/null
                    elif grep -q "RockPro64" /sys/firmware/devicetree/base/model 2>/dev/null; then
                        mpv -really-quiet -vo sdl -fs "$LAUNCHVIDS/system-default.mp4" </dev/tty &>/dev/null
                    else
                        mpv -slave -nogui -really-quiet -vo sdl -fs -zoom "$LAUNCHVIDS/system-default.mp4" </dev/tty &>/dev/null
                    fi
                else
                    return
                fi
            else
                fbi -1 -t "$IMAGE_DELAY" -noverbose -a "$image" </dev/tty &>/dev/null
            fi
        fi
    elif [[ "$DISABLE_MENU" -ne 1 && "$USE_ART" -ne 1 ]]; then
        local launch_name
        if [[ -n "$ROM_BN" ]]; then
            launch_name="$ROM_BN ($EMULATOR)"
        else
            launch_name="$EMULATOR"
        fi
        DIALOGRC="$CONFIGDIR/all/runcommand-launch-dialog.cfg" dialog --infobox "\nLaunching $launch_name ...\n\nPress a button to configure\n\nErrors are logged to $LOG" 9 60
    fi
}

function check_menu() {
    local dont_launch=0
    # check for key pressed to enter configuration
    IFS= read -s -t 2 -N 1 key </dev/tty
    if [[ -n "$key" ]]; then
        [[ -n "$IMG_PID" ]] && kill -SIGINT "$IMG_PID"
        tput cnorm
        main_menu
        dont_launch=$?
        tput civis
        clear
    fi
    return $dont_launch
}

# calls script with parameters SYSTEM, EMULATOR, ROM, and commandline
function user_script() {
    local script="$CONFIGDIR/all/$1"
    if [[ -f "$script" ]]; then
        bash "$script" "$SYSTEM" "$EMULATOR" "$ROM" "$COMMAND" </dev/tty 2>>"$LOG"
    fi
}

function restore_cursor_and_exit() {
    # if we are not being run from emulationstation (get parent of parent), turn the cursor back on.
    if [[ "$(ps -o comm= -p $(ps -o ppid= -p $PPID))" != "emulationstatio" ]]; then
        tput cnorm
    fi

    exit 0
}

function launch_command() {
    local ret
    # escape $ to avoid variable expansion (eg roms containing $!)
    COMMAND="${COMMAND//\$/\\\$}"
    # launch the command
    echo -e "Parameters: $@\nExecuting: $COMMAND" >>"$LOG"
    if [[ "$CONSOLE_OUT" -eq 1 ]]; then
        # turn cursor on
        tput cnorm
        eval $COMMAND </dev/tty 2>>"$LOG"
        ret=$?
        tput civis
    else
        eval $COMMAND </dev/tty &>>"$LOG"
        ret=$?
    fi
    return $ret
}
# BIOS Check Start TODO:  Port content
function bios_check() {

    BIOS="$HOME/ARES/BIOS"
    ROMS="$HOME/ARES/roms"
	
    if [[ "$SYSTEM" =~ ^("3do")$ ]]; then
        for filename in panafz10.bin; do
            if [[ ! -f "$BIOS/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy panafz10.bin  to the internal SD card:\n\n$BIOS/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi
	
	if [[ "$SYSTEM" =~ ^("amiga"|"amigacd32"|"amigacdtv"|"amiga500"|"amiga1200")$ ]]; then
        for filename in kick12.rom kick13.rom kick20.rom kick31.rom kick40063.A600 kick33180.A500 kick40068.A1200 kick34005.A500; do
            if [[ ! -f "$BIOS/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy kick12.rom kick13.rom kick20.rom kick31.rom kick40063.A600 kick33180.A500 kick40068.A1200 kick34005.A500 to the internal SD card:\n\n$BIOS/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi
	
	if [[ "$SYSTEM" =~ ^("apple2gs")$ ]]; then
        for filename in ROM1; do
            if [[ ! -f "$BIOS/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy ROM1 and ROM3 (optional)  to the internal SD card:\n\n$BIOS\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi
	
	if [[ "$SYSTEM" =~ ^("apple2e")$ ]]; then
        for filename in apple2.zip apple2ee.zip apple2e.zip apple2p.zip; do
            if [[ ! -f "$ROMS/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy apple2.zip apple2ee.zip apple2e.zip apple2p.zip  to the internal SD card:\n\n$ROMS/apple2e\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi
	
	if [[ "$SYSTEM" =~ ^("atari800"|"atari5200"|"atarixegs")$ ]]; then
        for filename in ATARIXL.ROM ATARIBAS.ROM ATARIOSA.ROM ATARIOSB.ROM 5200.ROM; do
            if [[ ! -f "$BIOS/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy ATARIXL.ROM ATARIBAS.ROM ATARIOSA.ROM ATARIOSB.ROM 5200.ROM to the internal SD card:\n\n$BIOS\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi
	
	if [[ "$SYSTEM" =~ ^("atarilynx")$ ]]; then
        for filename in lynxboot.img; do
            if [[ ! -f "$BIOS/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy lynxboot.img  to the internal SD card:\n\n$BIOS/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi
	
	if [[ "$SYSTEM" =~ ^("atarist")$ ]]; then
        for filename in tos.img; do
            if [[ ! -f "$BIOS/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy tos.img  to the internal SD card:\n\n$BIOS/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi
	
	if [[ "$SYSTEM" =~ ^("cdimono1")$ ]]; then
        for filename in cdimono1.zip; do
            if [[ ! -f "$ROMS/cdimono1/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy cdimono1.zip to the internal SD card:\n\n$ROMS/cdimono1\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi
	
	if [[ "$SYSTEM" =~ ^("channelf")$ ]]; then
        for filename in  sl31254.bin sl31253.bin sl90025.bin; do
            if [[ ! -f "$BIOS/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy  sl31254.bin sl31253.bin sl90025.bin to the internal SD card:\n\n$BIOS/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi
	
	if [[ "$SYSTEM" =~ ^("coco"|"dragon32")$ ]]; then
        for filename in bas13.rom; do
            if [[ ! -f "$BIOS/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy bas13.rom  to the internal SD card:\n\n$BIOS/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi
	
	if [[ "$SYSTEM" =~ ^("coleco")$ ]]; then
        for filename in coleco.rom; do
            if [[ ! -f "$BIOS/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy coleco.rom  to the internal SD card:\n\n$BIOS/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi
	
	if [[ "$SYSTEM" =~ ^("crvision")$ ]]; then
        for filename in bioscv.rom cslbios.rom cslbiossm.rom disk.rom laser2001.rom saloram.rom; do
            if [[ ! -f "$BIOS/crvision/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy bioscv.rom cslbios.rom cslbiossm.rom disk.rom laser2001.rom saloram.rom to the internal SD card:\n\n$BIOS/crvision\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi
	
	if [[ "$SYSTEM" =~ ^("dreamcast")$ ]]; then
        for filename in dc_boot dc_flash; do
            if [[ ! -f "$BIOS/dc/$filename.bin" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy dc_boot.bin and dc_flash.bin to the internal SD card:\n\n$BIOS/dc\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi
	
	if [[ "$SYSTEM" =~ ^("fds")$ ]]; then
        for filename in disksys.rom; do
            if [[ ! -f "$BIOS/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy disksys.rom  to the internal SD card:\n\n$BIOS/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi
	
    if [[ "$SYSTEM" =~ ^("intellivision")$ ]]; then
        for filename in exec.bin grom.bin ECS.BIN IVOICE.BIN; do
            if [[ ! -f "$BIOS/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy exec.bin, grom.bin ECS.BIN IVOICE.BIN  to the internal SD card:\n\n$BIOS/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi
	
	if [[ "$SYSTEM" =~ ^("macintosh")$ ]]; then
        for filename in mac.rom disk.img; do
            if [[ ! -f "$BIOS/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy mac.rom disk.img  to the internal SD card:\n\n$BIOS/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi
	
	if [[ "$SYSTEM" =~ ^("macplus")$ ]]; then
        for filename in vMac.ROM; do
            if [[ ! -f "$BIOS/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy vMac.ROM  to the internal SD card:\n\n$BIOS/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi
	
	if [[ "$SYSTEM" =~ ^("naomi"|"atomiswave")$ ]]; then
        for filename in airlbios awbios f355bios f355dlx hod2bios naomi; do
            if [[ ! -f "$BIOS/dc/$filename.zip" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy airlbios.zip, awbios.zip, f355bios.zip, f355dlx.zip, hod2bios.zip, and naomi.zip from the Mame BIOS pack to the internal SD card:\n\n$BIOS/dc\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
        for filename in naomi_boot naomi_boot_jp naomi_boot_us; do
            if [[ -f "$BIOS/dc/$filename.bin" ]]; then
                rm "$BIOS/dc/$filename.bin" &> /dev/null
            fi
        done
    fi
	
	if [[ "$SYSTEM" =~ ^("neocdz")$ ]]; then
        for filename in ng-lo.rom uni-bioscd.rom; do
            if [[ ! -f "$BIOS/neocd/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy ng-lo.rom uni-bioscd.rom to the internal SD card:\n\n$BIOS/neocd\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi
	
    if [[ "$SYSTEM" =~ ^("neogeo")$ ]]; then
        for filename in neogeo.zip; do
            if [[ ! -f "$ROMS/neogeo/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy neogeo.zip to the internal SD card:\n\n$ROMS/neogeo\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi
	
	if [[ "$SYSTEM" =~ ^("pc88")$ ]]; then
        for filename in N88.ROM N88SUB.ROM N88N.ROM N88EXT0.ROM N88EXT1.ROM N88EXT2.ROM N88EXT3.ROM n88.rom n88_0.rom n88_1.rom n88_2.rom n88_3.rom n88n.rom disk.rom n88knj1.rom n88knj2.rom n88jisho.rom; do
            if [[ ! -f "$BIOS/pc88/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy N88.ROM N88SUB.ROM N88N.ROM N88EXT0.ROM N88EXT1.ROM N88EXT2.ROM N88EXT3.ROM n88.rom n88_0.rom n88_1.rom n88_2.rom n88_3.rom n88n.rom disk.rom n88knj1.rom n88knj2.rom n88jisho.rom to the internal SD card:\n\n$BIOS/pc88\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi
	
    if [[ "$SYSTEM" =~ ^("pc98")$ ]]; then
        for filename in bios.rom font.rom itf.rom sound.rom 2608_bd.wav 2608_sd.wav 2608_top.wav 2608_hh.wav 2608_tom.wav 2608_rim.wav; do
            if [[ ! -f "$BIOS/np2kai/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy bios.rom font.rom itf.rom sound.rom 2608_bd.wav 2608_sd.wav 2608_top.wav 2608_hh.wav 2608_tom.wav 2608_rim.wav to the internal SD card:\n\n$BIOS/np2kai\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi

    if [[ "$SYSTEM" =~ ^("pcengine"|"supergrafx"|"pce-cd"|"tg16"|"tg-cd")$ ]]; then
        for filename in syscard3.pce; do
            if [[ ! -f "$BIOS/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy syscard3.pce to the internal SD card:\n\n$BIOS\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi
	
	if [[ "$SYSTEM" =~ ^("pcfx")$ ]]; then
        for filename in pcfx.rom; do
            if [[ ! -f "$BIOS/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy pcfx.rom  to the internal SD card:\n\n$BIOS/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi
	
	if [[ "$SYSTEM" =~ ^("psx")$ ]]; then
        for filename in scph101.bin scph7001.bin scph5501.bin scph1001.bin scph5502.bin; do
            if [[ ! -f "$BIOS/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy scph101.bin scph7001.bin scph5501.bin scph1001.bin scph5502.bin to the internal SD card:\n\n$BIOS/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi
	
	if [[ "$SYSTEM" =~ ^("saturn")$ ]]; then
        for filename in saturn_bios_us.bin saturn_bios_jp.bin saturn_bios.bin sega_101.bin mpr-17933.bin; do
            if [[ ! -f "$BIOS/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy saturn_bios_us.bin saturn_bios_jp.bin saturn_bios.bin sega_101.bin mpr-17933.bin to the internal SD card:\n\n$BIOS/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi
	
	if [[ "$SYSTEM" =~ ^("segacd")$ ]]; then
        for filename in us_scd1_9210.bin eu_mcd1_9210.bin jp_mcd1_9112.bin bios_CD_U.bin bios_CD_E.bin bios_CD_J.bin; do
            if [[ ! -f "$BIOS/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy us_scd1_9210.bin eu_mcd1_9210.bin jp_mcd1_9112.bin bios_CD_U.bin bios_CD_E.bin bios_CD_J.bin to the internal SD card:\n\n$BIOS/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi
	
	if [[ "$SYSTEM" =~ ^("ti99")$ ]]; then
        for filename in TI-994A.ctg; do
            if [[ ! -f "$BIOS/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy TI-994A.ctg and cf7+.ctg ti-disk.ctg spchrom.bin (optional) to the internal SD card:\n\n$BIOS\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi
	
	if [[ "$SYSTEM" =~ ^("trs-80-1")$ ]]; then
        for filename in level2.rom; do
            if [[ ! -f "$BIOS/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy level2.rom to the internal SD card:\n\n$BIOS\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi
	
    if [[ "$SYSTEM" =~ ^("trs-80-3")$ ]]; then
        for filename in level3.rom; do
            if [[ ! -f "$BIOS/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy level3.rom to the internal SD card:\n\n$BIOS\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi
	
    if [[ "$SYSTEM" =~ ^("trs-80-4")$ ]]; then
        for filename in level4.rom level4p.rom; do
            if [[ ! -f "$BIOS/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy level4.rom level4p.rom to the internal SD card:\n\n$BIOS\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi
	
	if [[ "$SYSTEM" =~ ^("videopac"|"odyssey2")$ ]]; then
        for filename in o2rom.bin c52.bin g7400.bin jopac.bin; do
            if [[ ! -f "$BIOS/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy o2rom.bin c52.bin g7400.bin jopac.bin to the internal SD card:\n\n$BIOS\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi
	
	if [[ "$SYSTEM" =~ ^("x1")$ ]]; then
        for filename in IPLROM.X1 IPLROM.X1T; do
            if [[ ! -f "$BIOS/xmil/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy IPLROM.X1 IPLROM.X1T to the internal SD card:\n\n$BIOS/xmil\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi
	
    if [[ "$SYSTEM" =~ ^("x68000")$ ]]; then
        for filename in iplrom.dat cgrom.dat; do
            if [[ ! -f "$BIOS/keropi/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy iplrom.dat cgrom.dat iplrom30.dat (optional), iplromco.dat (optional), iplromxv.dat (optional) to the internal SD card:\n\n$BIOS/keropi\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi
	
	if [[ "$SYSTEM" =~ ^("adam")$ ]]; then
        for filename in adam.zip adam_ddp.zip adam_fdc.zip adam_kb.zip adam_prn.zip adam_spi.zip; do
            if [[ ! -f "$ROMS/adam/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy adam.zip adam_ddp.zip adam_fdc.zip adam_kb.zip adam_prn.zip adam_spi.zip  to the internal SD card:\n\n$ROMS/adam/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi
	
if [[ "$SYSTEM" =~ ^("advision")$ ]]; then
        for filename in advision.zip; do
            if [[ ! -f "$ROMS/advision/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy advision.zip  to the internal SD card:\n\n$ROMS/advision/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi	
	
if [[ "$SYSTEM" =~ ^("alice")$ ]]; then
        for filename in alice.zip alice32.zip alice90.zip; do
            if [[ ! -f "$ROMS/alice/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy alice.zip alice32.zip alice90.zip  to the internal SD card:\n\n$ROMS/alice/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi	
	
if [[ "$SYSTEM" =~ ^("astrocde")$ ]]; then
        for filename in astrocde.zip astrocdl.zip astrocdw.zip; do
            if [[ ! -f "$ROMS/astrocde/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy astrocde.zip astrocdl.zip astrocdw.zip  to the internal SD card:\n\n$ROMS/astrocde/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi	
	
if [[ "$SYSTEM" =~ ^("atom")$ ]]; then
        for filename in atom.zip atombb.zip; do
            if [[ ! -f "$ROMS/atom/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy atom.zip atombb.zip  to the internal SD card:\n\n$ROMS/atom/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi	
	
if [[ "$SYSTEM" =~ ^("aquarius")$ ]]; then
        for filename in aquarius.zip; do
            if [[ ! -f "$ROMS/aquarius/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy aquarius.zip  to the internal SD card:\n\n$ROMS/aquarius/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi	
	
if [[ "$SYSTEM" =~ ^("apogee")$ ]]; then
        for filename in apogee.zip; do
            if [[ ! -f "$ROMS/apogee/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy apogee.zip  to the internal SD card:\n\n$ROMS/apogee/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi	
	
if [[ "$SYSTEM" =~ ^("cgenie")$ ]]; then
        for filename in cgenie.zip cgenie_fdc.zip cgenienz.zip; do
            if [[ ! -f "$ROMS/cgenie/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy cgenie.zip cgenie_fdc.zip cgenienz.zip  to the internal SD card:\n\n$ROMS/cgenie/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi		
	
if [[ "$SYSTEM" =~ ^("ep128")$ ]]; then
        for filename in ep128.zip ep64.zip ep64_exdos.zip; do
            if [[ ! -f "$ROMS/ep128/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy ep128.zip ep64.zip ep64_exdos.zip  to the internal SD card:\n\n$ROMS/ep128/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi		
	
if [[ "$SYSTEM" =~ ^("fmtowns")$ ]]; then
        for filename in fmtowns.zip; do
            if [[ ! -f "$ROMS/fmtowns/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy fmtowns.zip  to the internal SD card:\n\n$ROMS/fmtowns/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi	

if [[ "$SYSTEM" =~ ^("gamate")$ ]]; then
        for filename in gamate.zip; do
            if [[ ! -f "$ROMS/gamate/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy gamate.zip  to the internal SD card:\n\n$ROMS/gamate/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi		
	
if [[ "$SYSTEM" =~ ^("gamecom")$ ]]; then
        for filename in gamecom.zip; do
            if [[ ! -f "$ROMS/gamecom/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy gamecom.zip  to the internal SD card:\n\n$ROMS/gamecom/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi		
	
if [[ "$SYSTEM" =~ ^("gameking")$ ]]; then
        for filename in gameking.zip; do
            if [[ ! -f "$ROMS/gameking/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy gameking.zip  to the internal SD card:\n\n$ROMS/gameking/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi		
	
if [[ "$SYSTEM" =~ ^("gamepock")$ ]]; then
        for filename in gamepock.zip; do
            if [[ ! -f "$ROMS/gamepock/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy gamepock.zip  to the internal SD card:\n\n$ROMS/gamepock/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi		
	
if [[ "$SYSTEM" =~ ^("gmaster")$ ]]; then
        for filename in gmaster.zip; do
            if [[ ! -f "$ROMS/gmaster/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy gmaster.zip  to the internal SD card:\n\n$ROMS/gmaster/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi	

if [[ "$SYSTEM" =~ ^("hec2hrx")$ ]]; then
        for filename in hec2hrx.zip; do
            if [[ ! -f "$ROMS/hec2hrx/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy hec2hrx.zip  to the internal SD card:\n\n$ROMS/hec2hrx/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi	

	if [[ "$SYSTEM" =~ ^("camplynx")$ ]]; then
        for filename in lynx48k.zip lynx96k.zip lynx128k.zip; do
            if [[ ! -f "$ROMS/camplynx/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy lynx48k.zip lynx96k.zip lynx128k.zip  to the internal SD card:\n\n$ROMS/camplynx/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi		
	
if [[ "$SYSTEM" =~ ^("m5")$ ]]; then
        for filename in m5.zip m5p.zip m5p_brno.zip; do
            if [[ ! -f "$ROMS/m5/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy m5.zip m5p.zip m5p_brno.zip  to the internal SD card:\n\n$ROMS/m5/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi		
	
if [[ "$SYSTEM" =~ ^("mc10")$ ]]; then
        for filename in mc10.zip; do
            if [[ ! -f "$ROMS/mc10/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy mc10.zip  to the internal SD card:\n\n$ROMS/mc10/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi		
	
if [[ "$SYSTEM" =~ ^("lviv")$ ]]; then
        for filename in lviv.zip; do
            if [[ ! -f "$ROMS/lviv/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy lviv.zip  to the internal SD card:\n\n$ROMS/lviv/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi	

if [[ "$SYSTEM" =~ ^("mtx512")$ ]]; then
        for filename in mtx512.zip mtx_sdxcpm.zip mtx_sdxbas.zip; do
            if [[ ! -f "$ROMS/mtx512/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy mtx512.zip mtx_sdxcpm.zip mtx_sdxbas  to the internal SD card:\n\n$ROMS/mtx512/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi	

if [[ "$SYSTEM" =~ ^("mz700")$ ]]; then
        for filename in mz700.zip mz700j.zip; do
            if [[ ! -f "$ROMS/mz700/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy mz700.zip mz700j.zip to the internal SD card:\n\n$ROMS/mz700/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi

if [[ "$SYSTEM" =~ ^("mz2500")$ ]]; then
        for filename in mz2500.zip mz2520.zip; do
            if [[ ! -f "$ROMS/mz2500/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy mz2500.zip mz2520.zip to the internal SD card:\n\n$ROMS/mz2500/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi	
	
if [[ "$SYSTEM" =~ ^("pegasus")$ ]]; then
        for filename in pegasus.zip; do
            if [[ ! -f "$ROMS/pegasus/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy pegasus.zip  to the internal SD card:\n\n$ROMS/pegasus/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi	
	
if [[ "$SYSTEM" =~ ^("pockstat")$ ]]; then
        for filename in pockstat.zip; do
            if [[ ! -f "$ROMS/pockstat/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy pockstat.zip  to the internal SD card:\n\n$ROMS/pockstat/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi	
	
if [[ "$SYSTEM" =~ ^("pv2000")$ ]]; then
        for filename in pv2000.zip; do
            if [[ ! -f "$ROMS/pv2000/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy pv2000.zip  to the internal SD card:\n\n$ROMS/pv2000/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi	
	
if [[ "$SYSTEM" =~ ^("radio86")$ ]]; then
        for filename in radio86.zip; do
            if [[ ! -f "$ROMS/radio86/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy radio86.zip  to the internal SD card:\n\n$ROMS/radio86/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi	
	
if [[ "$SYSTEM" =~ ^("scv")$ ]]; then
        for filename in scv.zip; do
            if [[ ! -f "$ROMS/scv/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy scv.zip  to the internal SD card:\n\n$ROMS/scv/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi	
	
if [[ "$SYSTEM" =~ ^("socrates")$ ]]; then
        for filename in socrates.zip; do
            if [[ ! -f "$ROMS/socrates/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy socrates.zip  to the internal SD card:\n\n$ROMS/socrates/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi	
	
if [[ "$SYSTEM" =~ ^("sorcerer")$ ]]; then
        for filename in sorcerer.zip sorcererd.zip; do
            if [[ ! -f "$ROMS/sorcerer/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy sorcerer.zip sorcererd.zip  to the internal SD card:\n\n$ROMS/sorcerer/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi	
	
if [[ "$SYSTEM" =~ ^("tutor")$ ]]; then
        for filename in tutor.zip pyuuta.zip pyuutajr.zip; do
            if [[ ! -f "$ROMS/tutor/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy tutor.zip pyuuta.zip pyuutajr.zip to the internal SD card:\n\n$ROMS/tutor/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi		

if [[ "$SYSTEM" =~ ^("vg5k")$ ]]; then
        for filename in vg5k.zip; do
            if [[ ! -f "$ROMS/vg5k/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy vg5k.zip  to the internal SD card:\n\n$ROMS/vg5k/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi	
	
if [[ "$SYSTEM" =~ ^("apfimag")$ ]]; then
        for filename in apfm1000.zip; do
            if [[ ! -f "$ROMS/apfimag/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy apfm1000.zip  to the internal SD card:\n\n$ROMS/apfimag/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi	
	
if [[ "$SYSTEM" =~ ^("bbcbp128")$ ]]; then
        for filename in bbcb.zip bbcbp.zip; do
            if [[ ! -f "$ROMS/bbcbp128/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy bbcb.zip bbcbp.zip  to the internal SD card:\n\n$ROMS/bbcbp128/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi

	if [[ "$SYSTEM" =~ ^("electron")$ ]]; then
        for filename in electron.zip electron_m2105.zip electron_plus1.zip electron_plus3.zip electron64.zip electront.zip; do
            if [[ ! -f "$ROMS/electron/$filename" ]]; then
                dialog --no-ok --no-cancel --pause "REQUIRED BIOS FILES\n\nCopy electron.zip electron_m2105.zip electron_plus1.zip electron_plus3.zip electron64.zip electront.zip  to the internal SD card:\n\n$ROMS/electron/\n\nCheck https://github.com/Retro-Arena/RetroArena-Setup/wiki for more information." 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        done
    fi	
	
	if grep -q "ODROID-N2" /sys/firmware/devicetree/base/model 2>/dev/null; then
        if [[ "$SYSTEM" == "kodi" ]]; then
            rescheck=$(sudo cat /media/boot/boot.ini | grep -c 'setenv hdmimode "1080p60hz"')
            if [ $rescheck -eq 0 ]; then
                dialog --no-ok --no-cancel --pause 'IMPORTANT NOTE\n\nKodi Leia 18.3 requires the HDMI resolution set to 1080p.\n\nTo enable, go to Options in EmulationStation, launch RetroArena-Setup > Settings > Kodi then select the "Enable 1080p" option.\n\nPlease note that 720p will perform better in majority of emulators than 1080p. There is an option to revert back to 720p.' 22 76 15
                clear
                user_script "runcommand-onend.sh"
                exit 1
            fi
        fi
    fi
}
# BIOS Check End 

#OGST LCD START


function ogst_off() {
    if lsmod | grep -q 'fbtft_device'; then
        sudo rmmod fbtft_device &> /dev/null
    fi
}

function ogst_loading() {
    OGST="$HOME/ARES/casetheme"
    if ! lsmod | grep -q 'fbtft_device'; then
        #sudo modprobe fbtft_device name=hktft9340 busnum=1 rotate=270 &> /dev/null
        mplayer -quiet -nolirc -nosound -vo fbdev2:/dev/fb1 -vf scale=320:240 "$OGST/default.png" &> /dev/null
    else
        mplayer -quiet -nolirc -nosound -vo fbdev2:/dev/fb1 -vf scale=320:240 "$OGST/default.png" &> /dev/null
    fi
}

function ogst_play() {
    # wait until process loads
    if [[ $EMULATOR == advmame* ]]; then
        EMU_PROC="advmame"
    elif [[ $EMULATOR == amiberry* ]]; then
        EMU_PROC="amiberry-xu4"
    elif [[ $EMULATOR == basilisk ]]; then
        EMU_PROC="BasiliskII"
    elif [[ $EMULATOR == cgenius ]]; then
        EMU_PROC="CGeniusExe"
    elif [[ $EMULATOR == daphne ]]; then
        EMU_PROC="daphne.bin"
    elif [[ $EMULATOR == hatari* ]]; then
        EMU_PROC="hatari"
    elif [[ $EMULATOR == kodi ]]; then
        EMU_PROC="kodi.bin"
    elif [[ $EMULATOR == lr-* ]]; then
        EMU_PROC="retroarch"
    elif [[ $EMULATOR == mupen64plus* ]]; then
        EMU_PROC="mupen64plus"
    elif [[ $EMULATOR == openfodder ]]; then
        EMU_PROC="OpenFodder"
    elif [[ $EMULATOR == ppsspp ]]; then
        EMU_PROC="PPSSPPSDL"
    elif [[ $EMULATOR == quasi88 ]]; then
        EMU_PROC="quasi88.sdl"
    elif [[ $EMULATOR == reicast* ]]; then
        EMU_PROC="reicast"
    elif [[ $EMULATOR == sdlpop ]]; then
        EMU_PROC="prince"
    elif [[ $EMULATOR == solarus ]]; then
        EMU_PROC="solarus_run"
    elif [[ $EMULATOR == tyrquake ]]; then
        EMU_PROC="tyr-quake"
    elif [[ $EMULATOR == ti99sim ]]; then
        EMU_PROC="ti99sim-sdl"
    elif [[ $EMULATOR == vice-x64 ]]; then
        EMU_PROC="x64"
        SYSTEM="c64"
    elif [[ $EMULATOR == vice-x64sc ]]; then
        EMU_PROC="x64sc"
        SYSTEM="c64"
    elif [[ $EMULATOR == vice-x128 ]]; then
        EMU_PROC="x128"
        SYSTEM="c128"
    elif [[ $EMULATOR == vice-xpet ]]; then
        EMU_PROC="xpet"
        SYSTEM="pet"
    elif [[ $EMULATOR == vice-xplus4 ]]; then
        EMU_PROC="xplus4"
        SYSTEM="plus4"
    elif [[ $EMULATOR == vice-xvic* ]]; then
        EMU_PROC="xvic"
        SYSTEM="vic20"
    elif [[ $EMULATOR == xroar-coco* ]]; then
        EMU_PROC="xroar"
        SYSTEM="coco"
    elif [[ $EMULATOR == xroar-dragon32 ]]; then
        EMU_PROC="xroar"
        SYSTEM="dragon32"
    elif [[ $EMULATOR == yabause* ]]; then
        EMU_PROC="yabasanshiro"
    else
        EMU_PROC="$EMULATOR"
    fi  
    
    until pids=$(pidof $EMU_PROC)
    do
        sleep 1
    done
   
    # load image
    OGST="$HOME/ARES/casetheme"
    MNB_BA="$HOME/ARES/roms/$SYSTEM/boxart"
    MNB_CA="$HOME/ARES/roms/$SYSTEM/cartart"
    MNB_SP="$HOME/ARES/roms/$SYSTEM/snap"
    MNB_WL="$HOME/ARES/roms/$SYSTEM/wheel"
    SKY_MQ="$HOME/ARES/roms/$SYSTEM/media/marquees"
    SKY_SS="$HOME/ARES/roms/$SYSTEM/media/screenshots"
    SKP_MQ="$HOME/ARES/roms/$SYSTEM/media/marquee"
    SKP_SS="$HOME/ARES/roms/$SYSTEM/media/images"
    SLP_MQ="$HOME/ARES/roms/$SYSTEM/images"
    SLP_SS="$HOME/ARES/roms/$SYSTEM/images"
    
    for pid in $pids; do
        # wait
         sleep 1
               
        # turn on lcd if turned off
        if ! lsmod | grep -q 'fbtft_device'; then
            sudo modprobe fbtft_device name=hktft9340 busnum=1 rotate=270 &> /dev/null
        fi
        
        # clears the screen
        #mplayer -quiet -nolirc -nosound -vo fbdev2:/dev/fb1 -vf scale=320:240 "$OGST/blank.png" &> /dev/null
        
        if [[ -e "$HOME/.config/ogst001" ]]; then
            if [[ -e "$OGST/system-$SYSTEM.png" ]]; then
                mplayer -quiet -nolirc -nosound -vo fbdev2:/dev/fb1 -vf scale=320:240 "$OGST/system-$SYSTEM.png" &> /dev/null
            else
                mplayer -quiet -nolirc -nosound -vo fbdev2:/dev/fb1 -vf scale=320:240 "$OGST/default.png" &> /dev/null
            fi
        fi
        if [[ -e "$HOME/.config/ogst002" ]]; then
            if [[ -e "$MNB_BA/$ROM_BN.png" ]]; then
                mplayer -quiet -nolirc -nosound -vo fbdev2:/dev/fb1 -vf scale -zoom -xy 300 "$MNB_BA/$ROM_BN.png" &> /dev/null
            elif  [[ -e "$OGST/system-$SYSTEM.png" ]]; then
                mplayer -quiet -nolirc -nosound -vo fbdev2:/dev/fb1 -vf scale=320:240 "$OGST/system-$SYSTEM.png" &> /dev/null
            else
                mplayer -quiet -nolirc -nosound -vo fbdev2:/dev/fb1 -vf scale=320:240 "$OGST/default.png" &> /dev/null
            fi
        fi
        if [[ -e "$HOME/.config/ogst003" ]]; then
            if [[ -e "$MNB_CA/$ROM_BN.png" ]]; then
                mplayer -quiet -nolirc -nosound -vo fbdev2:/dev/fb1 -vf scale -zoom -xy 300 "$MNB_CA/$ROM_BN.png" &> /dev/null
            elif  [[ -e "$OGST/system-$SYSTEM.png" ]]; then
                mplayer -quiet -nolirc -nosound -vo fbdev2:/dev/fb1 -vf scale=320:240 "$OGST/system-$SYSTEM.png" &> /dev/null
            else
                mplayer -quiet -nolirc -nosound -vo fbdev2:/dev/fb1 -vf scale=320:240 "$OGST/default.png" &> /dev/null
            fi
        fi
        if [[ -e "$HOME/.config/ogst004" ]]; then
            if [[ -e "$MNB_SP/$ROM_BN.png" ]]; then
                mplayer -quiet -nolirc -nosound -vo fbdev2:/dev/fb1 -vf scale=320:240 "$MNB_SP/$ROM_BN.png" &> /dev/null
            elif  [[ -e "$OGST/system-$SYSTEM.png" ]]; then
                mplayer -quiet -nolirc -nosound -vo fbdev2:/dev/fb1 -vf scale=320:240 "$OGST/system-$SYSTEM.png" &> /dev/null
            else
                mplayer -quiet -nolirc -nosound -vo fbdev2:/dev/fb1 -vf scale=320:240 "$OGST/default.png" &> /dev/null
            fi
        fi
        if [[ -e "$HOME/.config/ogst005" ]]; then
            if [[ -e "$MNB_WL/$ROM_BN.png" ]]; then
                mplayer -quiet -nolirc -nosound -vo fbdev2:/dev/fb1 -vf scale -zoom -xy 300 "$MNB_WL/$ROM_BN.png" &> /dev/null
            elif  [[ -e "$OGST/system-$SYSTEM.png" ]]; then
                mplayer -quiet -nolirc -nosound -vo fbdev2:/dev/fb1 -vf scale=320:240 "$OGST/system-$SYSTEM.png" &> /dev/null
            else
                mplayer -quiet -nolirc -nosound -vo fbdev2:/dev/fb1 -vf scale=320:240 "$OGST/default.png" &> /dev/null
            fi
        fi
        if [[ -e "$HOME/.config/ogst006" ]]; then
            if [[ -e "$SKY_MQ/$ROM_BN.png" ]]; then
                mplayer -quiet -nolirc -nosound -vo fbdev2:/dev/fb1 -vf scale -zoom -xy 300 "$SKY_MQ/$ROM_BN.png" &> /dev/null
            elif  [[ -e "$OGST/system-$SYSTEM.png" ]]; then
                mplayer -quiet -nolirc -nosound -vo fbdev2:/dev/fb1 -vf scale=320:240 "$OGST/system-$SYSTEM.png" &> /dev/null
            else
                mplayer -quiet -nolirc -nosound -vo fbdev2:/dev/fb1 -vf scale=320:240 "$OGST/default.png" &> /dev/null
            fi
        fi
        if [[ -e "$HOME/.config/ogst007" ]]; then
            if [[ -e "$SKY_SS/$ROM_BN.png" ]]; then
                mplayer -quiet -nolirc -nosound -vo fbdev2:/dev/fb1 -vf scale=320:240 "$SKY_SS/$ROM_BN.png" &> /dev/null
            elif  [[ -e "$OGST/system-$SYSTEM.png" ]]; then
                mplayer -quiet -nolirc -nosound -vo fbdev2:/dev/fb1 -vf scale=320:240 "$OGST/system-$SYSTEM.png" &> /dev/null
            else
                mplayer -quiet -nolirc -nosound -vo fbdev2:/dev/fb1 -vf scale=320:240 "$OGST/default.png" &> /dev/null
            fi
        fi
        if [[ -e "$HOME/.config/ogst008" ]]; then
            if [[ -e "$SKP_MQ/$ROM_BN.png" ]]; then
                mplayer -quiet -nolirc -nosound -vo fbdev2:/dev/fb1 -vf scale -zoom -xy 300 "$SKP_MQ/$ROM_BN.png" &> /dev/null
            elif  [[ -e "$OGST/system-$SYSTEM.png" ]]; then
                mplayer -quiet -nolirc -nosound -vo fbdev2:/dev/fb1 -vf scale=320:240 "$OGST/system-$SYSTEM.png" &> /dev/null
            else
                mplayer -quiet -nolirc -nosound -vo fbdev2:/dev/fb1 -vf scale=320:240 "$OGST/default.png" &> /dev/null
            fi
        fi
        if [[ -e "$HOME/.config/ogst009" ]]; then
            if [[ -e "$SKP_SS/$ROM_BN.png" ]]; then
                mplayer -quiet -nolirc -nosound -vo fbdev2:/dev/fb1 -vf scale=320:240 "$SKP_SS/$ROM_BN.png" &> /dev/null
            elif  [[ -e "$OGST/system-$SYSTEM.png" ]]; then
                mplayer -quiet -nolirc -nosound -vo fbdev2:/dev/fb1 -vf scale=320:240 "$OGST/system-$SYSTEM.png" &> /dev/null
            else
                mplayer -quiet -nolirc -nosound -vo fbdev2:/dev/fb1 -vf scale=320:240 "$OGST/default.png" &> /dev/null
            fi
        fi
        if [[ -e "$HOME/.config/ogst010" ]]; then
            if [[ -e "$SLP_MQ/$ROM_BN-marquee.png" ]]; then
                mplayer -quiet -nolirc -nosound -vo fbdev2:/dev/fb1 -vf scale -zoom -xy 300 "$SLP_MQ/$ROM_BN-marquee.png" &> /dev/null
            elif  [[ -e "$OGST/system-$SYSTEM.png" ]]; then
                mplayer -quiet -nolirc -nosound -vo fbdev2:/dev/fb1 -vf scale=320:240 "$OGST/system-$SYSTEM.png" &> /dev/null
            else
                mplayer -quiet -nolirc -nosound -vo fbdev2:/dev/fb1 -vf scale=320:240 "$OGST/default.png" &> /dev/null
            fi
        fi
        if [[ -e "$HOME/.config/ogst011" ]]; then
            if [[ -e "$SLP_SS/$ROM_BN-image.jpg" ]]; then
                convert "$SLP_SS/$ROM_BN-image.jpg" "$SLP_SS/$ROM_BN-image.png"
                mplayer -quiet -nolirc -nosound -vo fbdev2:/dev/fb1 -vf scale=320:240 "$SLP_SS/$ROM_BN-image.png" &> /dev/null
                rm -rf "$SLP_SS/$ROM_BN-image.png"
            elif  [[ -e "$OGST/system-$SYSTEM.png" ]]; then
                mplayer -quiet -nolirc -nosound -vo fbdev2:/dev/fb1 -vf scale=320:240 "$OGST/system-$SYSTEM.png" &> /dev/null
            else
                mplayer -quiet -nolirc -nosound -vo fbdev2:/dev/fb1 -vf scale=320:240 "$OGST/default.png" &> /dev/null
            fi
        fi
    done
    
    # exit process
    while kill -0 "$pids" >/dev/null 2>&1; do
        sleep 1
    done

    if lsmod | grep -q 'fbtft_device'; then
        mplayer -quiet -nolirc -nosound -vo fbdev2:/dev/fb1 -vf scale=320:240 "$OGST/es.png" &> /dev/null
    else
        #sudo modprobe fbtft_device name=hktft9340 busnum=1 rotate=270 &> /dev/null
        mplayer -quiet -nolirc -nosound -vo fbdev2:/dev/fb1 -vf scale=320:240 "$OGST/es.png" &> /dev/null
    fi
}

function ogst_emu() {
    if ls /usr/local/share/ogst/ogst000 1> /dev/null 2>&1; then
        if [[ "$EMULATOR" =~ ^(lr.*|ppsspp)$ ]]; then
            ogst_loading
            ogst_play
        else
            ogst_off
            ogst_play
        fi
    else
        ogst_off
    fi
}
# END OGST LCD
	
	
function runcommand() {
    get_config

    if ! get_params "$@"; then
        echo "$0 MODE COMMAND [SAVENAME]"
        echo "$0 MODE _SYS_/_PORT_ SYSTEM ROM"
        exit 1
    fi

    # turn off cursor and clear screen
    tput civis
    clear

    rm -f "$LOG"
    echo -e "$SYSTEM\n$EMULATOR\n$ROM\n$COMMAND" >/dev/shm/runcommand.info
    user_script "runcommand-onstart.sh"
	bios_check
	ogst_emu &		  

    set_save_vars

    load_mode_defaults

    start_joy2key
    show_launch

    if [[ "$DISABLE_MENU" -ne 1 ]]; then
        if ! check_menu; then
            stop_joy2key
            user_script "runcommand-onend.sh"
            clear
            restore_cursor_and_exit 0
        fi
    fi
    stop_joy2key

    mode_switch "$MODE_REQ_ID"

    # replace X/Y resolution and refresh (useful for KMS/modesetting)
    COMMAND="${COMMAND//\%XRES\%/${MODE_CUR[2]}}"
    COMMAND="${COMMAND//\%YRES\%/${MODE_CUR[3]}}"
    COMMAND="${COMMAND//\%REFRESH\%/${MODE_CUR[5]}}"

    [[ -n "$FB_NEW" ]] && switch_fb_res $FB_NEW

    config_dispmanx "$SAVE_EMU"

    # switch to configured cpu scaling governor
    [[ -n "$GOVERNOR" ]] && set_governor "$GOVERNOR"

    retroarch_append_config

    # build xinitrc and rewrite command if not already in X11 context
    if [[ "$XINIT" -eq 1 && "$HAS_MODESET" != "x11" ]]; then
        build_xinitrc build
    fi

    local ret
    launch_command
    ret=$?

    [[ -n "$IMG_PID" ]] && kill -SIGINT "$IMG_PID"

    clear

    # remove tmp folder for unpacked archives if it exists
    rm -rf "/tmp/retroarch"

    # restore default cpu scaling governor
    [[ -n "$GOVERNOR" ]] && restore_governor

    # if we switched mode - restore preferred mode
    mode_switch "$MODE_ORIG_ID"

    # delete temporary xinitrc launch script
    if [[ "$XINIT" -eq 1 && "$HAS_MODESET" != "x11" ]]; then
        build_xinitrc clear
    fi

    # reset/restore framebuffer res (if it was changed)
    [[ -n "$FB_NEW" ]] && restore_fb

    [[ "$EMULATOR" == lr-* ]] && retroarchIncludeToEnd "$CONF_ROOT/retroarch.cfg"

    user_script "runcommand-onend.sh"

    restore_cursor_and_exit "$ret"
}

runcommand "$@"
