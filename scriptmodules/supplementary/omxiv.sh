#!/usr/bin/env bash

# This file is part of ARES by The RetroArena
#
# ARES is the legal property of its developers, whose names are
# too numerous to list here. Please refer to the COPYRIGHT.md file distributed with this source.
#
# See the LICENSE.md file at the top-level directory of this distribution and
# at https://raw.githubusercontent.com/Retro-Arena/RetroArena-Setup/master/LICENSE.md
#
# Core script functionality is based upon The RetroPie Project https://retropie.org.uk Script Modules
#

rp_module_id="omxiv"
rp_module_desc="OpenMAX image viewer for the Raspberry Pi"
rp_module_licence="GPL2 https://raw.githubusercontent.com/cmitu/omxiv/master/LICENSE"
rp_module_section="depends"
rp_module_flags="!all rpi"

function depends_omxiv() {
    getDepends libraspberrypi-dev libraspberrypi-doc libpng-dev libjpeg-dev
	if isPlatform "odroid-n2"; then
	/home/aresuser/ARES-Setup/fixmali.sh
    elif isPlatform "rockpro64"; then
    /usr/lib/arm-linux-gnueabihf/install_mali.sh
	fi
}

function sources_omxiv() {
    gitPullOrClone "$md_build" https://github.com/retropie/omxiv.git
}

function build_omxiv() {
    make clean
    make ilclient
    make
    md_ret_require="omxiv.bin"
}

function install_omxiv() {
    make install INSTALL="$md_inst"
}
